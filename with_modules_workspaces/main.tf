terraform {
  required_version = "0.13.1"
  backend "gcs" {
    credentials = "token.json"
    bucket      = "tf-module-workspace-code"
  }
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.0"
    }
  }
}
provider "google" {
  project     = var.project
  region      = "europe-west1"
  credentials = file("./token.json")
}

module "storage-permissions" {
  source              = "./modules/storage-permissions"
  id                  = var.id
  project             = var.project
//  dataset_permissions = var.dataset_permissions
}