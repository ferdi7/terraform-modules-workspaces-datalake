## Information

The `main.tf` hosts informations about the Terraform configuration as well as our module defintion and variables binding.

The files in the subfolder `modules/storage-permissions/` are the files to define the resources which will be deployed when running the Terraform plan and apply commands using a .tfvars file and within a Workspace.

The commands used to deploy the resources are :
```shell script
terraform fmt -recursive
terraform init -backend-config="prefix=workspaces/"
for i in "aaaaa" "bbbbb" "ccccc"; do 
terraform workspace select $i || terraform workspace new $i;
terraform plan -var-file=varfiles/$i.tfvars -out .terraform/plan-$i.out;
terraform apply ".terraform/plan-$i.out"
done;

```


