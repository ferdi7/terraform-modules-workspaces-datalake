variable "project" {
  type    = string
  default = "terraform-module-workspace"
}

variable "id" {
  type = string
}

variable "dataset_permissions" {
  type    = list(string)
  default = []
}