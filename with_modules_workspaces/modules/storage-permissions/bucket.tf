resource "google_storage_bucket" "config-bucket" {
  name                        = "${var.id}-config"
  location                    = "europe-west1"
  storage_class               = "REGIONAL"
  uniform_bucket_level_access = true
  labels                      = { id = var.id }
}

resource "google_storage_bucket" "input-bucket" {
  name                        = "${var.id}-input"
  location                    = "europe-west1"
  storage_class               = "REGIONAL"
  uniform_bucket_level_access = true
  labels                      = { id = var.id }
}

resource "google_storage_bucket" "archive-bucket" {
  name                        = "${var.id}-archive"
  location                    = "europe-west1"
  storage_class               = "REGIONAL"
  uniform_bucket_level_access = true
  labels                      = { id = var.id }
}

resource "google_storage_bucket_iam_binding" "sa-etl-read-transit-binding" {
  bucket  = google_storage_bucket.input-bucket.name
  members = ["serviceAccount:${google_service_account.sa-etl.email}"]
  role    = "roles/storage.objectAdmin"
}

resource "google_storage_bucket_iam_binding" "sa-etl-read-config-binding" {
  bucket  = google_storage_bucket.config-bucket.name
  members = ["serviceAccount:${google_service_account.sa-etl.email}"]
  role    = "roles/storage.objectViewer"
}

resource "google_storage_bucket_iam_binding" "sa-etl-read-write-archive-binding" {
  bucket  = google_storage_bucket.archive-bucket.name
  members = ["serviceAccount:${google_service_account.sa-etl.email}"]
  role    = "roles/storage.objectAdmin"
}