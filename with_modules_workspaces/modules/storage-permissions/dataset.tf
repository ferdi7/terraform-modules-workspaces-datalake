resource "google_bigquery_dataset" "dataset" {
  dataset_id = "${var.id}_result"
  location   = "EU"
  labels     = { id = var.id }
  access {
    role          = "OWNER"
    special_group = "projectOwners"
  }
  access {
    role          = "READER"
    special_group = "projectReaders"
  }
  access {
    role          = "WRITER"
    special_group = "projectWriters"
  }
  access {
    role          = "WRITER"
    user_by_email = google_service_account.sa-etl.email
  }
//  dynamic "access" {
//    for_each = toset(formatlist("%s-sa-etl@terraform-module-workspace.iam.gserviceaccount.com", var.dataset_permissions))
//    content {
//      role          = "READER"
//      user_by_email = access.value
//    }
//  }
}