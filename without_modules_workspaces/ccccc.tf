resource "google_storage_bucket" "cccc-input-bucket" {
  name = "cccc-input"
  location = "europe-west1"
  storage_class = "REGIONAL"
  uniform_bucket_level_access = true
  labels = { id = "cccc" }
}
resource "google_storage_bucket" "cccc-archive-bucket" {
  name = "cccc-archive"
  location = "europe-west1"
  storage_class = "REGIONAL"
  uniform_bucket_level_access = true
  labels = { id = "cccc" }
}
resource "google_storage_bucket" "cccc-config-bucket" {
  name = "cccc-config"
  location = "europe-west1"
  storage_class = "REGIONAL"
  uniform_bucket_level_access = true
  labels = { id = "cccc" }
}

resource "google_service_account" "cccc-sa-etl" {
  account_id = "cccc-sa-etl"
}

resource "google_storage_bucket_iam_binding" "cccc-etl-read-transit-binding" {
  bucket = google_storage_bucket.cccc-input-bucket.name
  members = ["serviceAccount:${google_service_account.cccc-sa-etl.email}"]
  role = "roles/storage.objectAdmin"
}
resource "google_storage_bucket_iam_binding" "cccc-etl-read-config-binding" {
  bucket = google_storage_bucket.cccc-config-bucket.name
  members = ["serviceAccount:${google_service_account.cccc-sa-etl.email}"]
  role = "roles/storage.objectViewer"
}
resource "google_storage_bucket_iam_binding" "cccc-etl-read-write-archive-binding" {
  bucket = google_storage_bucket.cccc-archive-bucket.name
  members = ["serviceAccount:${google_service_account.cccc-sa-etl.email}"]
  role = "roles/storage.objectAdmin"
}

resource "google_bigquery_dataset" "cccc-dataset" {
  dataset_id = "cccc_result"
  location = "EU"
  labels = { id = "cccc" }
  access {
    role          = "OWNER"
    special_group = "projectOwners"
  }
  access {
    role          = "READER"
    special_group = "projectReaders"
  }
  access {
    role          = "WRITER"
    special_group = "projectWriters"
  }
  access {
    role = "WRITER"
    user_by_email = google_service_account.cccc-sa-etl.email
  }
}