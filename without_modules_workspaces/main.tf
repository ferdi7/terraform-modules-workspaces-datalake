terraform {
  required_version = "0.13.1"
  backend "gcs" {
    credentials = "token.json"
    bucket      = "tf-mono-code"
  }
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.0"
    }
  }
}

provider "google" {
  project = "terraform-mono-code"
  region = "europe-west1"
  credentials = file("./token.json")
}


