resource "google_storage_bucket" "bbbb-input-bucket" {
  name = "bbbb-input"
  location = "europe-west1"
  storage_class = "REGIONAL"
  uniform_bucket_level_access = true
  labels = { id = "bbbb" }
}
resource "google_storage_bucket" "bbbb-archive-bucket" {
  name = "bbbb-archive"
  location = "europe-west1"
  storage_class = "REGIONAL"
  uniform_bucket_level_access = true
  labels = { id = "bbbb" }
}
resource "google_storage_bucket" "bbbb-config-bucket" {
  name = "bbbb-config"
  location = "europe-west1"
  storage_class = "REGIONAL"
  uniform_bucket_level_access = true
  labels = { id = "bbbb" }
}

resource "google_service_account" "bbbb-sa-etl" {
  account_id = "bbbb-sa-etl"
}

resource "google_storage_bucket_iam_binding" "bbbb-etl-read-transit-binding" {
  bucket = google_storage_bucket.bbbb-input-bucket.name
  members = ["serviceAccount:${google_service_account.bbbb-sa-etl.email}"]
  role = "roles/storage.objectAdmin"
}
resource "google_storage_bucket_iam_binding" "bbbb-etl-read-config-binding" {
  bucket = google_storage_bucket.bbbb-config-bucket.name
  members = ["serviceAccount:${google_service_account.bbbb-sa-etl.email}"]
  role = "roles/storage.objectViewer"
}
resource "google_storage_bucket_iam_binding" "bbbb-etl-read-write-archive-binding" {
  bucket = google_storage_bucket.bbbb-archive-bucket.name
  members = ["serviceAccount:${google_service_account.bbbb-sa-etl.email}"]
  role = "roles/storage.objectAdmin"
}

resource "google_bigquery_dataset" "bbbb-dataset" {
  dataset_id = "bbbb_result"
  location = "EU"
  labels = { id = "bbbb" }
  access {
    role          = "OWNER"
    special_group = "projectOwners"
  }
  access {
    role          = "READER"
    special_group = "projectReaders"
  }
  access {
    role          = "WRITER"
    special_group = "projectWriters"
  }
  access {
    role = "WRITER"
    user_by_email = google_service_account.bbbb-sa-etl.email
  }
}