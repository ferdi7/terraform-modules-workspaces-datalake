resource "google_storage_bucket" "aaaa-input-bucket" {
  name = "aaaa-input"
  location = "europe-west1"
  storage_class = "REGIONAL"
  uniform_bucket_level_access = true
  labels = { id = "aaaa" }
}
resource "google_storage_bucket" "aaaa-archive-bucket" {
  name = "aaaa-archive"
  location = "europe-west1"
  storage_class = "REGIONAL"
  uniform_bucket_level_access = true
  labels = { id = "aaaa" }
}
resource "google_storage_bucket" "aaaa-config-bucket" {
  name = "aaaa-config"
  location = "europe-west1"
  storage_class = "REGIONAL"
  uniform_bucket_level_access = true
  labels = { id = "aaaa" }
}

resource "google_service_account" "aaaa-sa-etl" {
  account_id = "aaaa-sa-etl"
}

resource "google_storage_bucket_iam_binding" "aaaa-etl-read-transit-binding" {
  bucket = google_storage_bucket.aaaa-input-bucket.name
  members = ["serviceAccount:${google_service_account.aaaa-sa-etl.email}"]
  role = "roles/storage.objectAdmin"
}
resource "google_storage_bucket_iam_binding" "aaaa-etl-read-config-binding" {
  bucket = google_storage_bucket.aaaa-config-bucket.name
  members = ["serviceAccount:${google_service_account.aaaa-sa-etl.email}"]
  role = "roles/storage.objectViewer"
}
resource "google_storage_bucket_iam_binding" "aaaa-etl-read-write-archive-binding" {
  bucket = google_storage_bucket.aaaa-archive-bucket.name
  members = ["serviceAccount:${google_service_account.aaaa-sa-etl.email}"]
  role = "roles/storage.objectAdmin"
}

resource "google_bigquery_dataset" "aaaa-dataset" {
  dataset_id = "aaaa_result"
  location = "EU"
  labels = { id = "aaaa" }
  access {
    role          = "OWNER"
    special_group = "projectOwners"
  }
  access {
    role          = "READER"
    special_group = "projectReaders"
  }
  access {
    role          = "WRITER"
    special_group = "projectWriters"
  }
  access {
    role = "WRITER"
    user_by_email = google_service_account.aaaa-sa-etl.email
  }
}